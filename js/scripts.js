(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		// DOM ready, take it away
		var art_too_big = function() { 
			$('.post-gallery .gallery-image').each(function(){
				if ($(this).width() > $(this).height()){
					//it's a landscape
					$(this).addClass('landscape');
				} else if ($(this).width() < $(this).height()){
					//it's a portrait
					$(this).addClass('portrait');
				} else {
					//image width and height are equal, therefore it is square.
					$(this).addClass('square');
				}
			});
		};
		art_too_big();
		
	});
	
})(jQuery, this);