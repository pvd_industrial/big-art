<?php get_header(); ?>

	<main role="main">
	<!-- section -->
	<section class="single-post clearfix">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
		<div class="post-thumbnail">
			<?php the_post_thumbnail(); ?>
			<?php edit_post_link('Edit Post'); ?>
		</div>
		<?php endif; ?>
		<!-- /post thumbnail -->

		<div class="wrapper clearfix">
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class('the-post'); ?>>

				<!-- post title -->
				<h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
				<!-- /post title -->

				<!-- post meta -->
				<div class="post-meta clearfix">
					<span class="post-date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
					<span class="author"><?php _e( 'By', 'bigart' ); ?> <?php the_author_posts_link(); ?></span>
					<span class="category"><?php _e( '', 'bigart' ); the_category(', ');?></span>
				</div><!-- /post meta -->

				<div class="post-content">
					<?php the_content(); ?>
					<div class="post-tags">
						<?php the_tags( __( 'Tags: ', 'bigart' ), ', '); ?>
					</div>
				</div>

				<?php comments_template(); ?>

			</article>
			<!-- /article -->

			<div class="post-sidebar">
				<?php
					if ( is_active_sidebar('sidebar-1') ) {
						dynamic_sidebar('sidebar-1');
					} else {
						get_sidebar();
					}
				?>
			</div>
		</div>

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'bigart' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>