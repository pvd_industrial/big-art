<?php get_header(); ?>

	<section id="home-slideshow" class="cycle-slideshow"
		data-cycle-fx="fade"
		data-cycle-timeout="5000"
		data-cycle-speed="1000"
		data-cycle-slides="> article.home-slide"
		data-pause-on-hover="true">

		<?php
			$latest_blog_posts = new WP_Query( array( 'posts_per_page' => 5 ) );

			if ( $latest_blog_posts->have_posts() ) : while ( $latest_blog_posts->have_posts() ) : $latest_blog_posts->the_post();
		?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class('home-slide'); ?>>

			<?php the_post_thumbnail('full'); ?>

			<div class="slide-content">
				<div class="wrapper">

				<h2><?php the_title(); ?></h2>

				<div class="slide-excerpt">
					<?php the_excerpt(); ?>
				</div>

				<a class="read-more link-button" href="<?php echo get_permalink($post->ID); ?>">Read More</a>

				</div><!-- /.wrapper -->
			</div><!-- /.slide-content -->

		</article>
		<!-- /article -->


		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'bigart' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

	</section>
	<main role="main">
		<section class="home-feature">
			<h2>New Art</h2>
		</section>
		<section class="home-feature">
			<h2>Artist Profile</h2>
		</section>
	</main>

<?php get_footer(); ?>